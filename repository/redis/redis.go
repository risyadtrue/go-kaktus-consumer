package repositoryRedis

import (
	"encoding/json"
	"go-kaktus-consumer/connection"
	"time"
)

func RedisSet(key string, data interface{}, expired time.Duration) (err error) {
	conn := connection.RedisConnection()
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return
	}

	err = conn.Set(key, string(dataBytes), expired).Err()
	if err != nil {
		return
	}

	return
}

func RedisGet(key string, data interface{}) (err error) {
	conn := connection.RedisConnection()
	redisData, err := conn.Get(key).Result()
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(redisData), &data)
	if err != nil {
		return
	}

	return
}
