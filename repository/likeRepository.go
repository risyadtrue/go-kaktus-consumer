package repository

import (
	"context"
	"database/sql"
	"go-kaktus-consumer/repository/dto"
)

type LikeRepository interface {
	Create(ctx context.Context, like dto.LikeRequest) (dto.LikeRequest, error)
}

type likeRepository struct {
	DB *sql.DB
}

func NewLikeRepository(db *sql.DB) LikeRepository {
	return &likeRepository{DB: db}
}

func (repository *likeRepository) Create(ctx context.Context, like dto.LikeRequest) (dto.LikeRequest, error) {
	var likeId int
	query := "INSERT INTO likes (user_id, forum_id) VALUES ($1, $2) RETURNING id"
	err := repository.DB.QueryRow(query, like.UserId, like.ForumId).Scan(&likeId)
	if err != nil {
		return like, err
	}
	like.Id = likeId

	return like, nil
}
