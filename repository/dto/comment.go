package dto

import (
	"database/sql"
	"time"
)

type CommentRequest struct {
	Id        int       `json:"id"`
	UserId    int       `json:"user_id"`
	ForumId   int       `json:"forum_id"`
	ParentId  int       `json:"parent_id"`
	Content   string    `json:"content"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type CommentResponse struct {
	Id         int               `json:"id"`
	ForumId    int               `json:"forum_id"`
	CommentBy  string            `json:"comment_by"`
	ParentId   sql.NullInt32     `json:"parent_id"`
	Comment    string            `json:"comment"`
	ReplyList  []CommentResponse `json:"reply_list"`
	TotalReply int               `json:"total_reply"`
	CreatedAt  time.Time         `json:"created_at"`
	UpdatedAt  time.Time         `json:"updated_at"`
}

type CommentFinalResponse struct {
	CommentBy  string                 `json:"comment_by"`
	Comment    string                 `json:"comment"`
	ReplyList  []CommentFinalResponse `json:"reply_list"`
	TotalReply int                    `json:"total_reply"`
	CreatedAt  time.Time              `json:"created_at"`
	UpdatedAt  time.Time              `json:"updated_at"`
}

type CommentWithRepliesResponse struct {
	CommentList []CommentResponse `json:"comment_list"`
	TotalReply  int               `json:"total_reply"`
}
