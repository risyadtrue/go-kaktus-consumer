package dto

type LikeRequest struct {
	Id      int `json:"id"`
	UserId  int `json:"user_id"`
	ForumId int `json:"forum_id"`
}
