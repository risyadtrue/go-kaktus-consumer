package repository

import (
	"context"
	"database/sql"
	"go-kaktus-consumer/repository/dto"
)

type UserRepository interface {
	Create(ctx context.Context, user dto.User) (dto.User, error)
	FindByEmail(ctx context.Context, email string) (dto.User, error)
	GetById(ctx context.Context, id int) (dto.User, error)
}

type userRepository struct {
	DB *sql.DB
}

// provider
func NewUserRepository(db *sql.DB) UserRepository {
	return &userRepository{DB: db}
}

func (repository *userRepository) Create(ctx context.Context, user dto.User) (dto.User, error) {
	lastInsertId := 0
	query := "INSERT INTO users(name, email, password, created_at, updated_at) VALUES ($1, $2, $3, $4, $5) RETURNING id"
	err := repository.DB.QueryRow(query, user.Name, user.Email, user.Password, user.CreatedAt, user.UpdatedAt).Scan(&lastInsertId)
	if err != nil {
		return user, err
	}

	user.Id = lastInsertId

	return user, nil
}

func (repository *userRepository) FindByEmail(ctx context.Context, email string) (dto.User, error) {
	var user dto.User
	query := "SELECT id, name, email, password, created_at FROM users WHERE email = $1"
	row, err := repository.DB.QueryContext(ctx, query, email)
	if err != nil {
		return user, err
	}

	if row.Next() {
		err := row.Scan(&user.Id, &user.Name, &user.Email, &user.Password, &user.CreatedAt)
		if err != nil {
			return user, nil
		}
	}

	return user, nil
}

func (repository *userRepository) GetById(ctx context.Context, id int) (dto.User, error) {
	var user dto.User
	query := "SELECT id, name, email FROM users where id = $1"
	row, err := repository.DB.QueryContext(ctx, query, id)
	if err != nil {
		return user, err
	}

	if row.Next() {
		row.Scan(&user.Id, &user.Name, &user.Email)
	}

	return user, nil
}
