package repository

import (
	"context"
	"database/sql"
	"go-kaktus-consumer/repository/dto"
)

type ForumRepository interface {
	Create(ctx context.Context, forum dto.Forum) (dto.Forum, error)
	GetAll(ctx context.Context) ([]dto.ForumResponse, error)
	GetByUserId(ctx context.Context, userId int) ([]dto.ForumResponse, error)
	GetById(ctx context.Context, forumId int) (dto.ForumDetailResponse, error)
}

type forumRepository struct {
	DB *sql.DB
}

func NewForumRepository(db *sql.DB) ForumRepository {
	return &forumRepository{DB: db}
}

func (repository *forumRepository) Create(ctx context.Context, forum dto.Forum) (dto.Forum, error) {
	var forumId int
	query := "INSERT INTO forums (user_id, title, description, created_at, updated_at) VALUES ($1, $2, $3, $4, $5) RETURNING id"
	err := repository.DB.QueryRow(query, forum.UserId, forum.Title, forum.Description, forum.CreatedAt, forum.UpdatedAt).Scan(&forumId)
	if err != nil {
		return forum, err
	}

	forum.Id = forumId

	return forum, nil

}

func (repository *forumRepository) GetAll(ctx context.Context) ([]dto.ForumResponse, error) {
	var forums []dto.ForumResponse
	query := "SELECT forums.id, users.name, forums.title, forums.description, forums.created_at FROM forums INNER JOIN users ON users.id = forums.user_id "
	rows, err := repository.DB.QueryContext(ctx, query)
	if err != nil {
		return forums, err
	}

	for rows.Next() {
		var forum dto.ForumResponse
		err := rows.Scan(&forum.Id, &forum.CreatedBy, &forum.Title, &forum.Description, &forum.CreatedAt)
		if err != nil {
			return forums, err
		}
		forums = append(forums, forum)
	}

	return forums, nil

}

func (repository *forumRepository) GetByUserId(ctx context.Context, userId int) ([]dto.ForumResponse, error) {
	var forums []dto.ForumResponse
	query := "SELECT forums.id, users.name, forums.title, forums.description, forums.created_at FROM forums INNER JOIN users ON users.id = forums.user_id WHERE user_id = $1"
	rows, err := repository.DB.QueryContext(ctx, query, userId)
	if err != nil {
		return forums, err
	}

	for rows.Next() {
		forum := dto.ForumResponse{}
		rows.Scan(&forum.Id, &forum.CreatedBy, &forum.Title, &forum.Description, &forum.CreatedAt)
		forums = append(forums, forum)
	}

	return forums, nil
}

func (repository *forumRepository) GetById(ctx context.Context, forumId int) (dto.ForumDetailResponse, error) {
	var forumDetail dto.ForumDetailResponse
	query := "SELECT forums.id, users.name AS created_by, forums.title, forums.description, COUNT(likes.user_id), forums.created_at FROM forums INNER JOIN users ON users.id = forums.user_id INNER JOIN likes ON forums.id = likes.forum_id WHERE forums.id = $1 GROUP BY forums.id, users.name, forums.title, forums.description"
	row, err := repository.DB.QueryContext(ctx, query, forumId)
	if err != nil {
		return forumDetail, err
	}

	if row.Next() {
		err := row.Scan(&forumDetail.Id, &forumDetail.CreatedBy, &forumDetail.Title, &forumDetail.Description, &forumDetail.TotalLikes, &forumDetail.CreatedAt)
		if err != nil {
			return forumDetail, err
		}
	}

	return forumDetail, nil
}
