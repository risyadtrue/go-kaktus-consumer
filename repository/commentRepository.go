package repository

import (
	"context"
	"database/sql"
	"go-kaktus-consumer/repository/dto"
	"log"
)

type CommentRepository interface {
	Create(ctx context.Context, comment dto.CommentRequest) (dto.CommentRequest, error)
	GetByForumId(ctx context.Context, forumId int) ([]dto.CommentResponse, error)
	CommentCount(ctx context.Context, forumId int) (int, error)
	GetByChildId(ctx context.Context, parentId int) ([]dto.CommentResponse, error)
}

type commentRepository struct {
	DB *sql.DB
}

func NewCommentRepository(db *sql.DB) CommentRepository {
	return &commentRepository{DB: db}
}

func (repository *commentRepository) Create(ctx context.Context, comment dto.CommentRequest) (dto.CommentRequest, error) {
	var commentId int

	query := "INSERT INTO comments (user_id, forum_id, parent_id, content, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id"
	if comment.ParentId == 0 {
		err := repository.DB.QueryRow(query, comment.UserId, comment.ForumId, nil, comment.Content, comment.CreatedAt, comment.UpdatedAt).Scan(&commentId)
		if err != nil {
			log.Fatal(err)

		}
	} else {
		err := repository.DB.QueryRow(query, comment.UserId, comment.ForumId, comment.ParentId, comment.Content, comment.CreatedAt, comment.UpdatedAt).Scan(&commentId)
		if err != nil {
			log.Fatal(err)
			return comment, err
		}
	}

	comment.Id = commentId

	return comment, nil
}

//return struct replieslist
// return struct commnetlist

func (repository *commentRepository) GetByForumId(ctx context.Context, forumId int) ([]dto.CommentResponse, error) {
	var commentResponses []dto.CommentResponse
	query := "SELECT comments.id, users.name, comments.forum_id, comments.parent_id, comments.content, comments.created_at, comments.updated_at FROM comments INNER JOIN users ON comments.user_id = users.id WHERE comments.forum_id = $1 AND comments.parent_id IS NULL"
	rows, err := repository.DB.QueryContext(ctx, query, forumId)
	if err != nil {
		return commentResponses, err
	}

	for rows.Next() {
		commentResponse := dto.CommentResponse{}
		//var commentReplies []dto.CommentResponse

		err := rows.Scan(&commentResponse.Id, &commentResponse.CommentBy, &commentResponse.ForumId, &commentResponse.ParentId, &commentResponse.Comment, &commentResponse.CreatedAt, &commentResponse.UpdatedAt)
		if err != nil {
			return commentResponses, nil
		}

		//query2 := "SELECT comments.id, users.name, comments.forum_id, comments.parent_id, comments.content, comments.created_at, comments.updated_at FROM comments INNER JOIN users ON comments.user_id = users.id WHERE comments.parent_id = $1 "
		//replyRows, err := repository.DB.QueryContext(ctx, query2, commentResponse.Id)
		//if err != nil {
		//	return commentResponses, err
		//}
		//for replyRows.Next() {
		//	replies := dto.CommentResponse{}
		//	err = replyRows.Scan(&replies.Id, &replies.CommentBy, &replies.ForumId, &replies.ParentId, &replies.Comment, &replies.CreatedAt, &replies.UpdatedAt)
		//	commentReplies = append(commentReplies, replies)
		//}
		//
		//commentResponse.ReplyList = commentReplies
		commentResponses = append(commentResponses, commentResponse)
	}

	return commentResponses, nil
}

func (repository *commentRepository) GetByChildId(ctx context.Context, parentId int) ([]dto.CommentResponse, error) {
	var commentResponses []dto.CommentResponse
	query := "SELECT comments.id, users.name, comments.forum_id, comments.parent_id, comments.content, comments.created_at, comments.updated_at FROM comments INNER JOIN users ON comments.user_id = users.id WHERE comments.parent_id = $1 "
	rows, err := repository.DB.QueryContext(ctx, query, parentId)
	if err != nil {
		return commentResponses, err
	}

	for rows.Next() {
		commentResponse := dto.CommentResponse{}
		err := rows.Scan(&commentResponse.Id, &commentResponse.CommentBy, &commentResponse.ForumId, &commentResponse.ParentId, &commentResponse.Comment, &commentResponse.CreatedAt, &commentResponse.UpdatedAt)
		if err != nil {
			return commentResponses, nil
		}

		commentResponses = append(commentResponses, commentResponse)
	}

	//fmt.Println(commentResponses)

	return commentResponses, nil
}

func (repository *commentRepository) CommentCount(ctx context.Context, forumId int) (int, error) {
	var commentsCount int
	query := "select count(user_id) from comments where forum_id = $1"
	row, err := repository.DB.QueryContext(ctx, query, forumId)
	if err != nil {
		return commentsCount, err
	}

	if row.Next() {
		err := row.Scan(&commentsCount)
		if err != nil {
			return commentsCount, err
		}
	}

	return commentsCount, nil
}
