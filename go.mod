module go-kaktus-consumer

go 1.17

require (
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.4
	github.com/streadway/amqp v1.0.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
)

require github.com/go-redis/redis v6.15.9+incompatible
