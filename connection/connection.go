package connection

import (
	"database/sql"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/streadway/amqp"
	"log"
	"os"
)

func PostgresSQLConnection() *sql.DB {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("cannot load .env file!")
	}

	connection := fmt.Sprintf("postgres://%s:%s@localhost/%s?sslmode=disable", os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_NAME"))
	connStr := connection
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	return db
}

func RedisConnection() (client *redis.Client) {
	client = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "",
		DB:       0,
	})
	
	return
}

func RabbitMQConnection() (ch *amqp.Channel, err error) {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		fmt.Println("cannot connect RabbitMQ")
	}
	//defer conn.Close()

	ch, err = conn.Channel()
	if err != nil {
		fmt.Println("error make channel")
	}
	//defer ch.Close()

	return
}
