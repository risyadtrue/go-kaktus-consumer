CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name varchar(100),
    email varchar(100),
    password varchar(100),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
)

CREATE TABLE forums (
    id SERIAL PRIMARY KEY,
    user_id int,
    title varchar(255),
    description text,
    created_at timestamp,
    updated_at timestamp,
    CONSTRAINT fk_forum FOREIGN KEY (user_id) REFERENCES users(id)
)

CREATE TABLE comments (
    id SERIAL PRIMARY KEY,
    user_id int,
    forum_id int,
    parent_id int,
    content text,
    created_at timestamp,
    updated_at timestamp,
    CONSTRAINT fk_comments_user FOREIGN KEY (user_id) REFERENCES users(id),
    CONSTRAINT fk_comments_forum FOREIGN KEY (forum_id) REFERENCES forums(id),
    CONSTRAINT fk_comments_parent FOREIGN KEY (parent_id) REFERENCES comments(id)
)

CREATE TABLE likes (
   id SERIAL PRIMARY KEY,
   user_id int,
   forum_id int,
   CONSTRAINT fk_like_user FOREIGN KEY (user_id) REFERENCES users(id),
   CONSTRAINT fk_like_forum FOREIGN KEY (forum_id) REFERENCES forums(id)
)