package utils

import (
	"go-kaktus-consumer/connection"
	"go-kaktus-consumer/repository"
	"go-kaktus-consumer/service"
)

func UserDependency() service.UserService {
	db := connection.PostgresSQLConnection()
	userRepository := repository.NewUserRepository(db)
	userService := service.NewUserService(userRepository)

	return userService
}

func ForumDependency() service.ForumService {
	db := connection.PostgresSQLConnection()
	forumRepository := repository.NewForumRepository(db)
	commentRepository := repository.NewCommentRepository(db)
	userRepository := repository.NewUserRepository(db)
	forumService := service.NewForumService(forumRepository, commentRepository, userRepository)

	return forumService
}

func LikeDependency() service.LikeService {
	db := connection.PostgresSQLConnection()
	likeRepository := repository.NewLikeRepository(db)
	likeService := service.NewLikeService(likeRepository)

	return likeService
}

func CommentDependency() service.CommentService {
	db := connection.PostgresSQLConnection()
	commentRepository := repository.NewCommentRepository(db)
	commentService := service.NewCommentService(commentRepository)

	return commentService
}
