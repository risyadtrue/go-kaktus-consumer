package service

import (
	"context"
	"encoding/json"
	"github.com/streadway/amqp"
	"go-kaktus-consumer/helper"
	"go-kaktus-consumer/repository"
	"go-kaktus-consumer/repository/dto"
	consumer "go-kaktus-consumer/service/consumer/helper"
	"log"
)

const (
	userForumLIkeQueueName = "userForumLike_queue"
)

type LikeService interface {
	Create(ctx context.Context) (err error)
}

type likeService struct {
	likeRepository repository.LikeRepository
}

func NewLikeService(likeRepository repository.LikeRepository) LikeService {
	return &likeService{likeRepository: likeRepository}
}

func (service *likeService) Create(ctx context.Context) (err error) {
	consumeData, ch, err := consumer.ConsumeFromQueue(userForumLIkeQueueName)
	if err != nil {
		return
	}
	var likedForum dto.LikeRequest

	foreverChannel := make(chan bool)
	go func() {
		for d := range consumeData {
			err := json.Unmarshal(d.Body, &likedForum)
			newLike, err := service.likeRepository.Create(ctx, likedForum)
			if err != nil {
				return
			}

			likeDataBytes, err := json.Marshal(newLike)
			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          likeDataBytes,
			})
			helper.FailOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request for like request")
	<-foreverChannel

	return
}
