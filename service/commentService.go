package service

import (
	"context"
	"encoding/json"
	"github.com/streadway/amqp"
	"go-kaktus-consumer/helper"
	"go-kaktus-consumer/repository"
	"go-kaktus-consumer/repository/dto"
	consumer "go-kaktus-consumer/service/consumer/helper"
	"log"
)

const (
	forumCommentQueueName = "forumComment_queue"
)

type CommentService interface {
	Create(ctx context.Context) (err error)
}

type commentService struct {
	commentRepository repository.CommentRepository
}

func NewCommentService(commentRepository repository.CommentRepository) CommentService {
	return &commentService{commentRepository: commentRepository}
}

func (service *commentService) Create(ctx context.Context) (err error) {
	consumeData, ch, err := consumer.ConsumeFromQueue(forumCommentQueueName)
	if err != nil {
		return
	}
	var commentRequest dto.CommentRequest

	foreverChannel := make(chan bool)
	go func() {
		for d := range consumeData {
			err := json.Unmarshal(d.Body, &commentRequest)
			newLike, err := service.commentRepository.Create(ctx, commentRequest)
			if err != nil {
				return
			}

			likeDataBytes, err := json.Marshal(newLike)
			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          likeDataBytes,
			})
			helper.FailOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request comment for selected forum")
	<-foreverChannel

	return
}
