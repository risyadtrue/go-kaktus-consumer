package service

import (
	"context"
	"encoding/json"
	"github.com/streadway/amqp"
	"go-kaktus-consumer/helper"
	"go-kaktus-consumer/repository"
	"go-kaktus-consumer/repository/dto"
	consumer "go-kaktus-consumer/service/consumer/helper"
	"golang.org/x/crypto/bcrypt"
	"log"
)

const (
	userQueueName      = "userData_queue"
	userLoginQueueName = "userLoginData_queue"
)

type UserService interface {
	RegisterUser(ctx context.Context) (err error)
	LoginUser(ctx context.Context) (err error)
}

type userService struct {
	userRepository repository.UserRepository
}

func NewUserService(userRepository repository.UserRepository) UserService {
	return &userService{userRepository: userRepository}
}

func (service *userService) RegisterUser(ctx context.Context) (err error) {
	consumeData, ch, err := consumer.ConsumeFromQueue(userQueueName)
	var userRequest dto.User

	foreverChannel := make(chan bool)

	go func() {
		for d := range consumeData {
			err := json.Unmarshal(d.Body, &userRequest)
			if err != nil {
				return
			}

			passwordHash, err := bcrypt.GenerateFromPassword([]byte(userRequest.Password), bcrypt.MinCost)
			if err != nil {
				return
			}
			userRequest.Password = string(passwordHash)

			newUser, err := service.userRepository.Create(ctx, userRequest)
			if err != nil {
				return
			}

			newUserBytes, err := json.Marshal(newUser)
			if err != nil {
				return
			}

			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          newUserBytes,
			})
			helper.FailOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request for register user")
	<-foreverChannel

	return

}

func (service *userService) LoginUser(ctx context.Context) (err error) {
	consumeData, ch, err := consumer.ConsumeFromQueue(userLoginQueueName)
	var userLoginRequest dto.UserLoginRequest
	var userBytes []byte

	foreverChannel := make(chan bool)

	go func() {
		for d := range consumeData {
			err := json.Unmarshal(d.Body, &userLoginRequest)

			user, err := service.userRepository.FindByEmail(ctx, userLoginRequest.Email)
			if err != nil {
				return
			}

			userBytes, err = json.Marshal(user)
			if err != nil {
				return
			}

			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          userBytes,
			})
			helper.FailOnError(err, "failed to publish")

			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request for user login")
	<-foreverChannel

	return

}
