package main

import (
	"context"
	"go-kaktus-consumer/utils"
)

func main() {
	ctx := context.Background()
	userService := utils.UserDependency()
	userService.RegisterUser(ctx)
}
