package main

import (
	"context"
	"go-kaktus-consumer/utils"
)

func main() {
	ctx := context.Background()
	forumService := utils.ForumDependency()
	forumService.Create(ctx)
}
