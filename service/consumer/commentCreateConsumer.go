package main

import (
	"context"
	"go-kaktus-consumer/utils"
)

func main() {
	ctx := context.Background()
	commentService := utils.CommentDependency()
	commentService.Create(ctx)
}
