package main

import (
	"context"
	"go-kaktus-consumer/utils"
)

func main() {
	ctx := context.Background()
	likeService := utils.LikeDependency()
	likeService.Create(ctx)
}
