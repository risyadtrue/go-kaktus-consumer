package helper

import (
	"github.com/streadway/amqp"
	"go-kaktus-consumer/connection"
	"go-kaktus-consumer/helper"
)

func ConsumeFromQueue(queueName string) (<-chan amqp.Delivery, *amqp.Channel, error) {
	ch, err := connection.RabbitMQConnection()
	if err != nil {
		return nil, ch, err
	}

	q, err := ch.QueueDeclare(queueName, false, false, true, false, nil)
	helper.FailOnError(err, "failed to declare queue")

	err = ch.Qos(1, 0, false)
	helper.FailOnError(err, "failed to set Qos")

	msgs, err := ch.Consume(q.Name, "", false, false, false, false, nil)

	return msgs, ch, nil
}
