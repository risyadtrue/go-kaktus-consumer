package service

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis"
	"github.com/streadway/amqp"
	"go-kaktus-consumer/helper"
	"go-kaktus-consumer/repository"
	"go-kaktus-consumer/repository/dto"
	repositoryRedis "go-kaktus-consumer/repository/redis"
	consumer "go-kaktus-consumer/service/consumer/helper"
	"log"
	"strconv"
	"time"
)

const (
	forumQueueName       = "forumData_queue"
	getForumQueueName    = "getForumData_queue"
	UserForumQueueName   = "getUserForumData_queue"
	ForumDetailQueueName = "getForumDetailData_queue"
)

type ForumService interface {
	Create(ctx context.Context) (err error)
	GetAll(ctx context.Context) (err error)
	GetByUserId(ctx context.Context)
	GetById(ctx context.Context) (err error)
	GetByIdRibet(ctx context.Context) (err error)
}

type forumService struct {
	forumRepository   repository.ForumRepository
	commentRepository repository.CommentRepository
	userRepository    repository.UserRepository
}

func NewForumService(forumRepository repository.ForumRepository, commentRepository repository.CommentRepository, userRepository repository.UserRepository) ForumService {
	return &forumService{forumRepository: forumRepository, commentRepository: commentRepository, userRepository: userRepository}
}

func (service *forumService) Create(ctx context.Context) (err error) {
	consumeData, ch, err := consumer.ConsumeFromQueue(forumQueueName)
	var forumRequest dto.Forum

	foreverChannel := make(chan bool)
	go func() {
		for d := range consumeData {
			err := json.Unmarshal(d.Body, &forumRequest)
			newForum, err := service.forumRepository.Create(ctx, forumRequest)
			if err != nil {
				return
			}

			// get forum data
			key := "allForum"
			var allForumData []dto.ForumResponse
			err = repositoryRedis.RedisGet(key, &allForumData)
			if err != nil {
				return
			}

			userData, err := service.userRepository.GetById(ctx, newForum.UserId)
			if err != nil {
				return
			}

			forumData := dto.ForumResponse{
				Id:          newForum.Id,
				CreatedBy:   userData.Name,
				Title:       newForum.Title,
				Description: newForum.Description,
				CreatedAt:   newForum.CreatedAt,
			}

			allForumData = append(allForumData, forumData)
			err = repositoryRedis.RedisSet(key, allForumData, 0)
			if err != nil {
				return
			}

			forumDataBytes, err := json.Marshal(newForum)
			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          forumDataBytes,
			})
			helper.FailOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request for create forum")
	<-foreverChannel

	return
}

func (service *forumService) GetAll(ctx context.Context) (err error) {
	key := "allForum"
	consumeData, ch, err := consumer.ConsumeFromQueue(getForumQueueName)
	var allForumData []dto.ForumResponse

	foreverChannel := make(chan bool)
	go func() {
		for d := range consumeData {
			err := repositoryRedis.RedisGet(key, &allForumData)
			if err != nil {
				if err == redis.Nil {
					// get forum from database
					allForumData, err = service.forumRepository.GetAll(ctx)
					if err != nil {
						return
					}

					// set data
					err = repositoryRedis.RedisSet(key, allForumData, 0)
					if err != nil {
						return
					}
				}
			}

			allForumDataResponse := dto.ForumAllResponse{
				ForumList:  allForumData,
				TotalForum: len(allForumData),
			}

			forumsDataBytes, err := json.Marshal(allForumDataResponse)
			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          forumsDataBytes,
			})
			helper.FailOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request for get all forum")
	<-foreverChannel

	return
}

func (service *forumService) GetByUserId(ctx context.Context) {
	consumeData, ch, _ := consumer.ConsumeFromQueue(UserForumQueueName)
	foreverChannel := make(chan bool)
	go func() {
		for d := range consumeData {
			userId, _ := strconv.Atoi(string(d.Body))
			userForums, err := service.forumRepository.GetByUserId(ctx, userId)
			if err != nil {
				return
			}

			forumsDataBytes, err := json.Marshal(userForums)
			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          forumsDataBytes,
			})

			helper.FailOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request for get forum by userId")
	<-foreverChannel
}

func (service *forumService) GetById(ctx context.Context) (err error) {
	consumeData, ch, err := consumer.ConsumeFromQueue(ForumDetailQueueName)
	if err != nil {
		return
	}
	var forumDetail dto.ForumDetailResponse

	foreverChannel := make(chan bool)
	go func() {
		for d := range consumeData {
			forumId, _ := strconv.Atoi(string(d.Body))
			forumDetail, err = service.forumRepository.GetById(ctx, forumId)

			parentCommentsData, _ := service.commentRepository.GetByForumId(ctx, forumId)
			nestData := nestedComment(ctx, &parentCommentsData, service.commentRepository)
			forumDetail.CommentList = nestData
			forumDetail.TotalComment = len(nestData)

			likeDataBytes, err := json.Marshal(forumDetail)
			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          likeDataBytes,
			})
			helper.FailOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request detail forum")
	<-foreverChannel

	return
}

func nestedComment(ctx context.Context, parentCommentsData *[]dto.CommentResponse, service repository.CommentRepository) []dto.CommentResponse {
	var commentData []dto.CommentResponse
	for _, data := range *parentCommentsData {
		repliesData, _ := service.GetByChildId(ctx, data.Id)
		repData := nestedComment(ctx, &repliesData, service)
		data.ReplyList = repData
		data.TotalReply = len(repData)
		commentData = append(commentData, data)
	}

	return commentData
}

func (service *forumService) GetByIdRibet(ctx context.Context) (err error) {
	consumeData, ch, err := consumer.ConsumeFromQueue(ForumDetailQueueName)
	if err != nil {
		return
	}
	var forumDetail dto.ForumDetailResponse

	foreverChannel := make(chan bool)
	go func() {
		for d := range consumeData {
			forumId, _ := strconv.Atoi(string(d.Body))
			key := "detailForum" + string(d.Body)

			err := repositoryRedis.RedisGet(key, &forumDetail)
			if err != nil {
				if err == redis.Nil {
					// get forum detail
					forumDetail, err = service.forumRepository.GetById(ctx, forumId)

					// get comment for detail forum
					commentDetails, _ := service.commentRepository.GetByForumId(ctx, forumId)
					if err != nil {
						return
					}

					var replies []dto.CommentResponse
					var commentParent []dto.CommentResponse
					//refCommentParent := &commentParent
					for _, data := range commentDetails {
						//commentWithReply := dto.CommentWithRepliesResponse{}
						//reply := dto.CommentResponse{}
						if !data.ParentId.Valid {
							//commentWithReply.CommentList = data
							commentParent = append(commentParent, data)
						}

						if data.ParentId.Int32 != 0 {
							replies = append(replies, data)
						}

					}

					var commentsAndReplies dto.CommentWithRepliesResponse
					for i := 0; i < len(commentParent); i++ {
						//var replyParent dto.CommentResponse
						var countReplies int
						for _, replyData := range replies {
							if replyData.ParentId.Int32 == int32(commentParent[i].Id) {
								commentParent[i].ReplyList = append(commentParent[i].ReplyList, replyData)
								countReplies++
								//commentParent = append(commentParent, replyData)
								//fmt.Println(replyData)
								//fmt.Println("-------------------------")
							}
							commentParent[i].TotalReply = countReplies
						}
						commentsAndReplies.CommentList = append(commentsAndReplies.CommentList, commentParent[i])
					}

					commentCount, _ := service.commentRepository.CommentCount(ctx, forumId)

					forumDetail.CommentList = commentsAndReplies.CommentList
					forumDetail.TotalComment = commentCount

					// set data
					err = repositoryRedis.RedisSet(key, forumDetail, time.Minute*5)
					if err != nil {
						return
					}
				}
			}

			likeDataBytes, err := json.Marshal(forumDetail)
			err = ch.Publish("", d.ReplyTo, false, false, amqp.Publishing{
				ContentType:   "text/plain",
				CorrelationId: d.CorrelationId,
				Body:          likeDataBytes,
			})
			helper.FailOnError(err, "failed to publish")
			d.Ack(false)
		}
	}()

	log.Println("Waiting RPC Request detail forum")
	<-foreverChannel

	return
}
